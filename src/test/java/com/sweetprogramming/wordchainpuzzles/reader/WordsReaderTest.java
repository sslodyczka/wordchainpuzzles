package com.sweetprogramming.wordchainpuzzles.reader;

/**
 * @author Stanislaw Slodyczka
 */
public class WordsReaderTest extends WordsReaderDefault {
    private static final String WORDLISTTEST_FILENAME = "wordlisttest.txt"; // smallest dictionary for quick graphs create

    public WordsReaderTest() {
        super(WORDLISTTEST_FILENAME);
    }
}
