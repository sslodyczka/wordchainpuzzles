package com.sweetprogramming.wordchainpuzzles;

import com.sweetprogramming.wordchainpuzzles.reader.WordsReaderTest;
import com.sweetprogramming.wordchainpuzzles.util.WordUtil;
import org.junit.Test;

import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author Stanislaw Slodyczka
 */
public class WordChainPuzzlesTest {
    private static final String START_WORD = "java";
    private static final String END_WORD = "knew";

    private final WordChainPuzzles wordChainPuzzles = new WordChainPuzzles(new WordsReaderTest());

    public WordChainPuzzlesTest() throws IOException {
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowNullPointerExceptionIfBothNull() {
        wordChainPuzzles.findWordChain(null, null);
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowNullPointerExceptionIfFirstNull() {
        wordChainPuzzles.findWordChain(null, END_WORD);
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowNullPointerExceptionIfSecondNull() {
        wordChainPuzzles.findWordChain(START_WORD, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionWhenWordsHaveDiffLength() {
        wordChainPuzzles.findWordChain(START_WORD, "black");
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionWhenWordIsNotInDictionary() {
        wordChainPuzzles.findWordChain("vvvv", END_WORD);
    }

    @Test
    public void shouldReturnFirstWordOnBegining() {
        assertEquals(START_WORD, wordChainPuzzles.findWordChain(START_WORD, END_WORD).get(0));
    }

    @Test
    public void shouldReturnSecondWordOnEnd() {
        List<String> chain = wordChainPuzzles.findWordChain(START_WORD, END_WORD);
        assertEquals(END_WORD, chain.get(chain.size() - 1));
    }

    @Test
    public void shouldWordsDontDuplicate() {
        Set<String> set = new HashSet<>();
        for (String each : wordChainPuzzles.findWordChain(START_WORD, END_WORD)) {
            assertTrue(set.add(each));
        }
    }

    @Test
    public void shouldAllWordsHadOneLength() {
        assertTrue(wordChainPuzzles.findWordChain(START_WORD, END_WORD)
                .stream()
                .findAny()
                .filter(word -> word.length() != 3).isPresent());
    }

    @Test
    public void shouldWordsDiffByOnlyOneChar() {
        List<String> chain = wordChainPuzzles.findWordChain(START_WORD, END_WORD);
        Iterator<String> firstIterator = chain.iterator();
        Iterator<String> secondIterator = chain.iterator();
        secondIterator.next();
        while (secondIterator.hasNext()) {
            assertTrue(WordUtil.isWordsOnlyDiffByOneChar(firstIterator.next(), secondIterator.next()));
        }
    }

    @Test
    public void shouldChainHaveSameLengthIfWordSwap() {
        List<String> chain = wordChainPuzzles.findWordChain(START_WORD, END_WORD);
        List<String> swapChain = wordChainPuzzles.findWordChain(END_WORD, START_WORD);
        assertEquals(chain.size(), swapChain.size());
    }
}
