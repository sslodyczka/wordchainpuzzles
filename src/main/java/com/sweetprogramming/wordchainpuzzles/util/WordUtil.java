package com.sweetprogramming.wordchainpuzzles.util;

/**
 * @author Stanislaw Slodyczka
 */
public class WordUtil {
    public static boolean isWordsOnlyDiffByOneChar(String first, String second) {
        final char[] firstArray = first.toCharArray();
        final char[] secondArray = second.toCharArray();
        int diffNumber = 0;
        for (int i = 0; i < firstArray.length; i++) {
            if (firstArray[i] != secondArray[i]) {
                diffNumber++;
            }
        }
        return diffNumber == 1;
    }
}
