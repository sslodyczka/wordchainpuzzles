package com.sweetprogramming.wordchainpuzzles;

import com.sweetprogramming.wordchainpuzzles.reader.WordsReaderDefault;

import java.io.IOException;
import java.util.Scanner;

/**
 * @author Stanislaw Slodyczka
 *         Simple and naive console interface for finding chain
 */
public class Main {
    private static final String WORDLIST_FILENAME = "wordlist.txt";

    public static void main(String[] args) {
        System.out.println("Loading dictionary...");
        try {
            WordChainPuzzles wordChainPuzzles = new WordChainPuzzles(new WordsReaderDefault(WORDLIST_FILENAME));
            String first = null;
            String second = null;
            while (!("exit99".equals(first) || "exit99".equals(second))) {
                Scanner scanner = new Scanner(System.in);
                System.out.print("First word (write \"exit99\" to close program): ");
                first = scanner.next();
                System.out.print("SecondWord (write \"exit99\" to close program): ");
                second = scanner.next();
                System.out.println(wordChainPuzzles.findWordChain(first, second));
            }
        } catch (IOException e) {
            System.out.println("Cannot read dictionary");
            e.printStackTrace();
        }
    }
}
