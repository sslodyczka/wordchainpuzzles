package com.sweetprogramming.wordchainpuzzles.reader;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;

/**
 * @author Stanislaw Slodyczka
 */
public class WordsReaderDefault implements WordsReader {
    private final String dictionaryFileName;

    public WordsReaderDefault(String dictionaryFileName) {
        this.dictionaryFileName = dictionaryFileName;
    }

    public Map<Integer, Collection<String>> readWordsMap() throws IOException {
        final Multimap<Integer, String> wordsMap = ArrayListMultimap.create();
        try {
            Files.lines(Paths.get(
                    ClassLoader.getSystemResource(dictionaryFileName).toURI()))
                    .forEach(word -> wordsMap.put(word.length(), word));
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return Collections.unmodifiableMap(wordsMap.asMap());
    }
}
