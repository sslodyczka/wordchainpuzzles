package com.sweetprogramming.wordchainpuzzles.reader;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;

/**
 * @author Stanislaw Slodyczka
 */
public interface WordsReader {
    Map<Integer, Collection<String>> readWordsMap() throws IOException;
}
