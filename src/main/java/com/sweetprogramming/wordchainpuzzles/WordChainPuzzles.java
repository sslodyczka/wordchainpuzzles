package com.sweetprogramming.wordchainpuzzles;

import com.sweetprogramming.wordchainpuzzles.check.WordChecker;
import com.sweetprogramming.wordchainpuzzles.graph.GraphCreator;
import com.sweetprogramming.wordchainpuzzles.reader.WordsReader;
import org.jgrapht.UndirectedGraph;
import org.jgrapht.alg.shortestpath.DijkstraShortestPath;
import org.jgrapht.graph.DefaultEdge;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author Stanislaw Slodyczka
 */
public class WordChainPuzzles {
    private final Map<Integer, Collection<String>> WORDS;
    private final Map<Integer, UndirectedGraph<String, DefaultEdge>> graphMap;
    private final WordChecker wordChecker;

    WordChainPuzzles(WordsReader wordsReader) throws IOException {
        WORDS = wordsReader.readWordsMap();
        graphMap = new GraphCreator().getIntegerUndirectedGraphMap(WORDS);
        wordChecker = new WordChecker(WORDS);
    }

    public List<String> findWordChain(String first, String second) {
        wordChecker.checkWords(first, second);
        return DijkstraShortestPath.findPathBetween(graphMap.get(first.length()), first, second).getVertexList();
    }
}
