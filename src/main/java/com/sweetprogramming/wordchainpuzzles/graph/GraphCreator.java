package com.sweetprogramming.wordchainpuzzles.graph;

import com.sweetprogramming.wordchainpuzzles.util.WordUtil;
import org.jgrapht.UndirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Stanislaw Slodyczka
 */
public class GraphCreator {

    public Map<Integer, UndirectedGraph<String, DefaultEdge>> getIntegerUndirectedGraphMap(Map<Integer, Collection<String>> wordMap) {
        final Map<Integer, UndirectedGraph<String, DefaultEdge>> graphMap = new HashMap<>(wordMap.size());
        wordMap.keySet().forEach(numberOfChar -> graphMap.put(numberOfChar, new SimpleGraph<>(DefaultEdge.class)));

        wordMap.forEach((number, words) -> {
            words.forEach(word -> graphMap.get(number).addVertex(word));
            words.forEach(word -> words.stream()
                    .filter(string -> WordUtil.isWordsOnlyDiffByOneChar(string, word))
                    .forEach(wordForEdge -> graphMap.get(number).addEdge(word, wordForEdge)));
        });

        return graphMap;
    }
}
