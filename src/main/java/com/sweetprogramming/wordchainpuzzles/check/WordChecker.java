package com.sweetprogramming.wordchainpuzzles.check;

import java.util.Collection;
import java.util.Map;

import static com.sweetprogramming.wordchainpuzzles.check.CheckStatus.*;

/**
 * @author Stanislaw Slodyczka
 */
public class WordChecker {
    private final Map<Integer, Collection<String>> words;

    public WordChecker(Map<Integer, Collection<String>> words) {
        this.words = words;
    }

    private CheckStatus checkWordsStatus(String first, String second) {
        if (first == null || second == null) {
            return IS_NULL;
        }
        if (first.length() != second.length()) {
            return DIFF_LENGTH;
        }
        if (isNotInDictionary(first, second)) {
            return NOT_IN_DICTIONARY;
        }
        return IS_VALID;
    }

    public boolean isWordsCorrect(String first, String second) {
        CheckStatus status = checkWordsStatus(first, second);
        return IS_VALID.equals(status);
    }

    public boolean checkWords(String first, String second) {
        CheckStatus status = checkWordsStatus(first, second);
        if (IS_NULL.equals(status)) {
            throw new NullPointerException();
        }
        if (DIFF_LENGTH.equals(status)) {
            throw new IllegalArgumentException("diff words length");
        }
        if (NOT_IN_DICTIONARY.equals(status)) {
            throw new IllegalArgumentException("Not in dictionary");
        }
        return IS_VALID.equals(status);
    }


    private boolean isNotInDictionary(String first, String second) {
        if (!words.containsKey(first.length())) {
            return true;
        }
        Collection<String> words = this.words.get(first.length());
        return !(words.contains(first) && words.contains(second));
    }
}
