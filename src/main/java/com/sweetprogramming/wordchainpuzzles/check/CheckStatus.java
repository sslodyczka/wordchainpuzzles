package com.sweetprogramming.wordchainpuzzles.check;

/**
 * @author Stanislaw Slodyczka
 */
public enum CheckStatus {
    IS_VALID, IS_NULL, DIFF_LENGTH, NOT_IN_DICTIONARY
}
